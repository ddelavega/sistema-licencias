import { environment } from 'src/environments/environment';

export const API_KEY = environment.apiKey;
export const baseUrl = environment.production ? 'https://api.jsonbin.io/b/5f0887eb5d4af74b0129dd77' : 'http://localhost:3000';
export const productsUrl = baseUrl + '/products';
export const cartUrl = baseUrl + '/cart';
export const wishlistUrl = baseUrl + '/wishlist';
