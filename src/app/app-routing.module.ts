import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministrationComponent } from './components/administration/administration.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'login', loadChildren: './login/login.module#LoginModule' },
  { path: 'license', loadChildren: './license/license.module#LicenseModule' },
  { path: 'backoffice', loadChildren: './backoffice/backoffice.module#BackofficeModule' },
  { path: 'onBoarding/:cuil/:role', loadChildren: './onBoarding/onBoarding.module#OnBoardingModule' },
  { path: 'token', loadChildren: './token/token.module#TokenModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
