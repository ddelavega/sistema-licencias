import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.sass']
})
export class AdministrationComponent implements OnInit {
  datos$;
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.datos$ = this.dataService.getLicences();
  }

}
