import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { baseUrl, API_KEY } from 'src/app/config/api';
import { Observable } from 'rxjs';
import { Licence } from './../../models/licence';


@Injectable({
  providedIn: 'root',
})
export class DataService {
  apiUrl = baseUrl;
  apiKey = API_KEY;

  constructor(private http: HttpClient) {}

  getLicence(licenceId: string): Observable<any> {
    const url = this.apiUrl;
    return this.http.get<any>(url);
  }

  getLicences(): Observable<any[]> {
    const headers = new HttpHeaders().append('secret-key', `${this.apiKey}`);
    console.log('SECTRET', this.apiKey);
    console.log('URL', this.apiUrl);
    return this.http.get<any[]>(this.apiUrl, { headers });
  }
}
